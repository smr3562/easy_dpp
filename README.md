# EASY Digital Pulse Processor

## Description
A small version of a Digital Pulse Processor for the smr3562 School. 
There are two groups of files, one for an ideal pulse processor (square signals) and the other for ECAL scintillator signal model.

## DPP for square ideal pulses

For the first part of the project the main idea is to have a minimal version of a DPP capable of
measure the amplitude of square ideal pulses.

All the required files are in the folder **dpp_0**. The synthesizable files are under **dpp_0/src** and the 
simulation files under **dpp_0/simul**. The files for the SDK are under **dpp_0/sdk**

## DPP for ECAL like pulses

For the second part of the project the participants will have to implement an upgraded version of a DPP capable of
measure the amplitude of pulses synthesized with a script for the ECAL2 COMPASS model.

All the required files are in the folder **dpp_1**. The synthesizable files are under **dpp_0/src**, the 
simulation files under **dpp_0/simul** and the python script under **dpp_0/soft**.



# General instructions to generate the project 

## Vivado
* Create a new project:
    * Name: YourProjectName - Location: ../vivado/ignore
    * RTL project, do not specify sources at this time
    * Boards, ZEDBOARD
* Import block design :
    * In Project Manager go to Settings -> IP -> Repository -> Comblock (Look 
      for comblock IP in your system)
    * Add sources -> Add all vhdl files under ./src
    * Tcl console: `source <path_from_project_base>/easy_dpp/dpp_X/easy_dpp_X.tcl`  (Replace the X with the actual name)
    * Right click over design1 -> Create HDL wrapper
* Generate Bitstream
* File -> Export -> Export Hardware (include the bitstream)
* File -> Launch SDK

## SDK

* File -> New -> Application Project
    * Name: DPP_xx (recommended)
    * OS Platform: standalone
    * hello world
    * Finish
* In src -> replace helloworld with the one provided in ./sdk
* Rigth click over DPP_xx -> Clean Project, Build Project