![image](doc/img/smr3562.png)

-------

Prepared by
Bruno Valinoti
ICTP-MLAB

-------

# Digital Pulse Processor, Light version 0

This first version of the DPP is for processing ideal square pulses,
the IP takes a stream of data from a FIFO (Comblock FIFO Out), 
detects the arrival of a pulse with a first order derivative
when its value (derivative) esceeds a threshold values defined 
by the user in runtime and, stores the amplitude value in another
FIFO (Comblock In) to be read afterwards.

## Main objectives of the DPP V0

<img src="doc/img/sq_pulses.png" alt="drawing" width="600"/>

1. Detect the arrival of the pulse
2. Measure the amplitude of a pulse
3. Store the amplitude in a FIFO

## Procedure
1. Generate the project in Vivado for the Zedboard 
2. Include the Processing System
3. Include the Comblock IP Core
4. Include the hdls sources for the easy_dpp_0
5. Open the included hdls and interpret the behavior
6. Add the DPP module to the block design
7. Generate a testbench/feeder for the DPP design
    1. Add all the simulation sources to the design
    2. Change the threshold values until you have only 5 pulses to be written in the fifo
    3. Explore the different operation modes
8. Connect the signals of the DPP in the block design in order to be able to control it with the Comblock
9. Generate bitstream, export it and open the SDK
10. Using the “hello world” template test the DPP feeding it through the Comblock’s FIFO and print the values of the pulses

For the steps 1 and 2 you can follow the guide https://gitlab.com/smr3562/labs/-/wikis/Lab-6:-Comblock until
section 3.10 (Double-click on the ComBlock block to open the configuration properties).

Create the "HDL Wrapper"

Select the next configuration options for the comblock
<img src="doc/img/comblock_config.png" alt="drawing" width="600"/>

Include the source files provided under **dpp_0/src/**

![image1](doc/img/src_hdls.png)

Now insert the DPP_module_top_0 in the block design, making right click on

![image2](doc/img/include_bd.png)

and select "Add Module to block design".

Once the DPP module is inserted in the bd, connect the signals as in the next figure:

![image3](doc/img/bd_connections.png)

### Simulation

1. Add all the files under **dpp_0/simul** to the "Simulation sources" in the sources window.

2. In the "Project Manager" make click on **Run Simulation**

3. Modify the parameters the DPP accepts and see the change in the behaviour (op_mode_i, threshold_i, ecc..)

### Implementation in the Zedboard

1. Generate the output products and the bitstream

2. Export hardware

3. Launch SDK

4. Create application project, in standalone way for the "hello world"

5. Replace the **helloworld.c** with the one we provide in the **dpp_0/sdk** directory.
 



