----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    05:04:38 09/11/2015 
-- Design Name: 
-- Module Name:    top_macromodule - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: This IP takes a real-like pulse, detects the arrival of 
--      it and stores the maximum amplitude value in a simple way with a digital
--      maximum holder.
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_signed.all;

entity DPP_module_top_1 is
      generic ( D_WIDTH: natural:=12);  --data width 
   Port (    
      --Global signals
      clk_i      : in  std_logic;
      rst_i      : in  std_logic;
      ena_i      : in  std_logic;
      ADC_data_i : in  std_logic_vector (11 downto 0); --From FAMU/others ADC
	--Input parameters
      cf_1_i : in std_logic_vector(4 downto 0);
      cf_2_i : in std_logic_vector(4 downto 0);
      cf_3_i : in std_logic_vector(4 downto 0);

      threshold_high_i : in  std_logic_vector (15 downto 0);
      threshold_low_i  : in  std_logic_vector (15 downto 0);
      op_mode_i        : in  std_logic_vector (1 downto 0);
      delay_tap_i      : in  std_logic_vector (15 downto 0);       
      data_o           : out std_logic_vector (15 downto 0);
    -------				
      fifo_wr_en_mux_out: out std_logic
);
			  
end DPP_module_top_1;

architecture Behavioral of DPP_module_top_1 is
--Signals
   signal FIR_Derivative_data : std_logic_vector(15 downto 0) ; --This is the data stream out of the derivative filter
   signal photon_arrival      : std_logic; --
   signal short_rejection_flag: std_logic;
   signal FIFO_wr_en          : std_logic; --
   signal FIR_data_out        : std_logic_vector(15 downto 0) ; ----Main_fir data out
   signal fifo_wr_en_mux      : std_logic; --
   signal FIFO_DATA_IN_mux    : STD_LOGIC_VECTOR (15 downto 0);
   signal valid_der           : std_logic;

   

   signal clr_dly1            : std_logic;         -- delays the clear signal 
   signal dly_count           : unsigned(15 downto 0);
   signal photon_dly          : std_logic;         -- delays the wr_en signal 
   signal count_ena           : std_logic;
   ------------------------------------------
   component fir_derivative_1 is 
   generic ( 
	   d_width: natural:=12);  --data width  
   Port ( 
      clk_i  :  in std_logic;
	  rst_i  :  in std_logic;
	  ena_i  :  in std_logic;
      data_i :  in std_logic_vector (d_width-1 downto 0);
      cf_1_i :  in std_logic_vector (4 downto 0);
      cf_2_i :  in std_logic_vector (4 downto 0);
      cf_3_i :  in std_logic_vector (4 downto 0);
      der_sign : out std_logic;
	  data_o : out std_logic_vector (15 downto 0)); -- one bit is gained with derivative
   end component;
   ---------------------------------------
   
begin
---- Definition of Operation modes

 --  FIR_data_top <= FIR_data_out;
 --  FIR_Deriv_data_top <= FIR_Derivative_data;

-- COMPONENTS INSTANTIATION SECTION
   my_op_mode_dec: entity work.output_mux
    port map(
    op_mode_i => op_mode_i,
    fifo_wr_en_i => FIFO_wr_en,
    FIR_data_i => FIR_data_out,
    ADC_data_i => ADC_data_i,
    FIR_deriv_i => FIR_Derivative_data,
    fifo_wr_en_o => FIFO_wr_en_mux,
    data_o => FIFO_DATA_IN_mux
    );

    data_o <= FIFO_DATA_IN_mux;

    fir_derivative : fir_derivative_1
      port map (
         clk_i  => clk_i, 
         rst_i  => rst_i,
         ena_i  => ena_i,
         data_i => ADC_data_i, 
         cf_1_i => cf_1_i,
         cf_2_i => cf_2_i,
         cf_3_i => cf_3_i,
         der_sign => valid_der,
         data_o => FIR_Derivative_data  
   );
	
   edge_detector : entity work.edge_detector_1
      port map (
         clk_i  => clk_i, 
         rst_i  => rst_i,
         ena_i  => valid_der, 
         data_i => FIR_Derivative_data, 
         ---
         threshold_high_i =>  threshold_high_i, 
         threshold_low_i  =>  threshold_high_i,
         -- 
         arrival_flag_o   => photon_arrival, 
         rejection_flag_o => short_rejection_flag
   );

   max_detector: entity work.max_holder   -- This is the 
      generic map ( D_WIDTH => D_WIDTH )
      port map( 
         clk_i  => clk_i,
         rst_i  => rst_i,
--         ena_i  => ena_i,
         clr_i  => clr_dly1,
         data_i => ADC_data_i,
         data_o => FIR_data_out(D_WIDTH-1 downto 0)
      );


   do_delay_tap: process (clk_i)
      begin
      if rising_edge(clk_i) then
         if rst_i = '1' then 
            count_ena <= '0';
            dly_count <= (others => '0');
         else 
            photon_dly <= '0';
            if photon_arrival = '1' then 
               count_ena <= '1';
               if delay_tap_i = "0000000000000000" then 
                  dly_count <= x"0000";
               else 
                  dly_count <= unsigned(delay_tap_i)-1;  --Will count 'till delay_tap_i + 1
               end if;         
            elsif dly_count > 0 then
               dly_count <= dly_count - 1;
            else
               dly_count <= dly_count;
            end if;
            
            if delay_tap_i = x"0000" and photon_arrival = '1' then
               photon_dly <= '1';
               count_ena <= '0';   
            elsif dly_count = 0 and count_ena = '1' then
               photon_dly <= '1';
               count_ena <= '0';
            end if;
            
          end if;
      end if;    
   end process do_delay_tap;
   
   
   do_clr_delay : process (clk_i)
      begin
      if rising_edge(clk_i) then
         clr_dly1 <= photon_dly;
      else 
         clr_dly1 <= clr_dly1;
      end if;
   end process do_clr_delay;

 ---  FIR_data_out <= "0000" & ADC_data_i;  -- TODO: Apply the filter
   FIR_data_out(FIR_data_out'LEFT downto D_WIDTH) <= (others => '0'); 
   fifo_wr_en_mux_out <= fifo_wr_en_mux;
--   FIFO_wr_en <= photon_arrival;  --TODO: Delay the "arrival_flag" the latency of the main FIR (138)
   FIFO_wr_en <= photon_dly;
end Behavioral;