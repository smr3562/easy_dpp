![image](img/smr3562.png)

-------

Prepared by
Bruno Valinoti
ICTP-MLAB

-------

# Digital Pulse Processor, Light version 1

This first version of the DPP is for processing pulses created 
as the ECAL2 COMPASS detectors, the IP takes a stream of data 
from a FIFO (Comblock FIFO Out), detects the arrival of a pulse 
with a mean derivative and when its value (derivative) exceeds 
ther threshold values defined by the user in runtime and, stores 
the amplitude value in another FIFO (Comblock In) to be read 
afterwards.

## Main objectives of the DPP V1

![image](img/pulses.png)

1. Detect the arrival of the pulse
2. Measure the amplitude of a pulse
3. Store the amplitude in a FIFO

## Procedure
1. Generate the project in Vivado for the Zedboard, 
when asked for the design files add all the files in
**dpp_1/src/**
3. Include the Comblock IP Core
4. In the TCL console go to **<path_to_dpp_1>** and run **source dpp_1.tcl**
5. Create the "HDL Wrapper" of the **dpp_1** and after the wrapper generation select "Set as top".
6. Generate the output products
7. Open the included hdls and interpret the behavior

The Block design should be as in the next figure

![image3](img/bd_connections.png)

### Simulation

1. Add all the files under **dpp_1/simul** to the "Simulation sources" in the sources window.

2. In the "Project Manager" make click on **Run Simulation**

3. Modify the parameters the DPP accepts and see the change in the behaviour (op_mode_i, threshold_i, ecc..)

### Implementation in the Zedboard

1. Generate the output products and the bitstream

2. Export hardware

3. Launch SDK

4. Create application project, in standalone way for the "hello world"

5. Replace the **helloworld.c** and include the **ecal_pulse.h** with those provided in the **dpp_1/sdk** directory.
 
### Extras

1. Implement the programmable tap delay (This to control when the write enable should activate 
the fifo writing) with a comblock resource
2. Implement a peak detector using only the derivative value
3. Group the control signals in one register
4. Pack the DPP into a Vivado IP



